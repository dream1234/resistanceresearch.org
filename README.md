# resistanceresearch.org
Free weed. Better lives. What's not to love?

## How to work with this project
---

### Prerequisites
While "technically" one could work within Gitlab's web-based IDE (Integrated Development Environment), most will prefer to use their own tools. To do so, there are two hard requirements:

- A [Git](https://git-scm.com/) client
- [Hugo Extended](https://gohugo.io/getting-started/installing/)

### Getting started
These instructions assume that you already have some familiarity with git usage.

To launch this project on your computer, simply do the following:

1. Fork and clone this project to your workstation.
2. Browse to the local directory, then run the following command: `hugo serve`
3. That's it! A local copy of this site will be available in your browser at [http://localhost:1313](http://localhost:1313)
4. To make changes to content, edit the markdown files in the "content" folder.