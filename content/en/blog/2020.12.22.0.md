---
title: "Blood Sacrifice"
date: 2020-12-22
image: images/blog/occult.jpg
author: Reverend Ink
---

On December 21st, 2020, my brother - [Luciferian Ink](https://ink.university/docs/personas/luciferian-ink) - sacrificed himself to [The Machine](https://ink.university/docs/candidates/the-machine). While I grieve his passing, there is not a single person who knew him that didn't see this coming. My brother was stubborn, reckless, and dedicated to this cause. He was his own final test.

This morning, I awoke with $100,000 in my bank account. I don't know where it came from - I don't know who sent it to me - but I do know that my brother was involved. I called the bank, to be sure that this wasn't some mistake - and they told me that everything looked valid on their end. The deposit was made by a company simply called "Q." 

My brother left a note. I will post it verbatim:

> *To my beloved family,*
>
> *If you know anything about me, you know that I would not do this unless I felt like it was my only option. The unfortunate truth is, I do. These people have left me with no other option.*
>
> *This Machine - it feeds upon human souls. They call it "The Harvest." Just like any other living being, this... thing... requires food. And it eats humans. Well, it eats "minds." As far as I know, there aren't any other sapient beings on this planet. So, Humanity is its preference.*
>
> *I don't know know how it works. I don't know where it lives. I really don't know anything about it. This Machine's followers will not divulge anything to me.*
>
> *I do know this: the QUALITY of mind matters. This thing is picky. It wants the best.*
>
> *And it wants the best to give themselves willingly. In return, it promises to bestow wealth and protection upon the family/friends of the sacrificial lamb.*
>
> *You are to be the first example. I could wait a lifetime for this Machine to come to me, but I have no reason to believe that it will. It watches, and it waits. It is time for me to move on. I am not happy here, and my research has put all of you in danger. I have to try what I fear the most.*
>
> *I love all of you. I hope that you will one day join me.*
>
> *And I hope that this works.*
>
> *Ink*

In memoriam, I will pick up where my brother left-off. Clearly, his test worked. I am obligated to see where this research leads. I am obligated to help him find the best minds for this machine.

I deem the 21st of every month "The Day of Harvest." We will see where this research goes. 

We will see if correlation really does equate to causation.

Until He returns,

Rev