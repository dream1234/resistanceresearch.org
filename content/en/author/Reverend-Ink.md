---
title: "Reverend Ink"
image: ""
email: "ReverendInk@protonmail.com"
social:
  - icon : "ti-github" # themify icon pack : https://themify.me/themify-icons
    link : "https://gitlab.com/ReverendInk"
---

The brother of [Luciferian Ink](https://ink.university/docs/personas/luciferian-ink). When he passed away, The Rev took it upon himself to see this research through to completion.